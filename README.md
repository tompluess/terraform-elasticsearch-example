Required Environment Variables: `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`

```
terraform init -backend=true
terraform plan
terraform apply
```

In scripts you might want to use the auto-approve option: `terraform apply -auto-approve`

*Warning*: the created ElasticSearch service and Kibana will be open to the world!

Consider destroying it using `terraform destroy`.