# Configure the S3 backend
terraform {
  required_version = "> 0.11.0"
  backend "s3" {
    bucket = "terraform-eu-central.skyr.ch"
    key    = "blubber/blubber.tfstate"
    region = "eu-central-1"
  }
}


# Define variables. Credentials and identifiers are loaded from terraform.tfvars
variable "region" {
  default = "eu-central-1"
}

# Configure the AWS Provider
provider "aws" {
  region     = "${var.region}"
}

resource "aws_cognito_user_pool" "kibana-users" {
  name = "kibana-users"
}

resource "aws_cognito_user_pool_domain" "main" {
  domain = "kibana-users-domain"
  user_pool_id = "${aws_cognito_user_pool.kibana-users.id}"
}

resource "aws_cognito_user_pool_client" "client" {
  name = "client"
  user_pool_id = "${aws_cognito_user_pool.kibana-users.id}"
}

resource "aws_cognito_identity_pool" "kibana-identity" {
  identity_pool_name               = "identity pool for kibana"
  allow_unauthenticated_identities = false

  supported_login_providers {
    "graph.facebook.com" = "7346241598935555"
  }
}

resource "aws_iam_role" "authenticated" {
  name = "cognito_authenticated"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.kibana-identity.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "authenticated" {
  name = "authenticated_policy"
  role = "${aws_iam_role.authenticated.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "cognito-sync:*",
        "cognito-identity:*",
        "es:AmazonESCognitoAccess"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF
}

resource "aws_cognito_identity_pool_roles_attachment" "kibana-identity" {
  identity_pool_id = "${aws_cognito_identity_pool.kibana-identity.id}"

  role_mapping {
    identity_provider         = "graph.facebook.com"
    ambiguous_role_resolution = "AuthenticatedRole"
    type                      = "Rules"

    mapping_rule {
      claim      = "isAdmin"
      match_type = "Equals"
      role_arn   = "${aws_iam_role.authenticated.arn}"
      value      = "paid"
    }
  }

  roles {
    "authenticated" = "${aws_iam_role.authenticated.arn}"
  }
}

resource "aws_iam_role" "kibana-role" {
  name = "kibana-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "kibana-policy" {
  name = "kibana-policy"
  role = "${aws_iam_role.kibana-role.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [{
      "Effect": "Allow",
      "Action": [
        "ec2:DescribeVpcs",
        "cognito-identity:ListIdentityPools",
        "cognito-idp:ListUserPools",
        "cognito-idp:describeIdentityPool",
        "cognito-idp:describeUserPool"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iam:GetRole",
        "iam:PassRole"
      ],
      "Resource": "${aws_iam_role.kibana-role.arn}"
    }
  ]
}
EOF
}

# Elasticsearch/Kibana for blubber IC Mail Event Logger
resource "aws_elasticsearch_domain" "es-blubber" {
  domain_name           = "es-blubber"
  elasticsearch_version = "5.3"
  cluster_config {
    instance_type = "t2.small.elasticsearch"
    instance_count = 1
  }
  ebs_options {
    ebs_enabled = true
    volume_size = 10 # GB
  }

  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
  }

  cognito_options {
    "enabled" = true
    "user_pool_id" = "${aws_cognito_user_pool.kibana-users.id}"
    "identity_pool_id" = "${aws_cognito_identity_pool.kibana-identity.id}"
    "role_arn" = "${aws_iam_role.kibana-role.arn}"
  }

  access_policies = <<CONFIG
{
    "Version": "2012-10-17",
    "Statement": [
        {
          "Action": "es:*",
          "Principal": {
            "AWS": "arn:aws:sts::360828181266:assumed-role/kibana-role/CognitoIdentityCredentials"
          },
          "Effect": "Allow"
       }
    ]
}
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags {
    Domain = "blubber.skyr.ch"
  }
}